import { createServer } from "http";
import * as process from "process";

async function main() {
  const httpServer = createServer();
  const port = process.env.BACKEND_PORT || 3000;
  httpServer.listen(port, () => {
    console.log(`Backend port started on ${port}`);
  });
}

main().catch((e) => {
  console.error("Fatal error occurred starting server!");
  console.error(e);
  process.exit(101);
});
