# Boilerplate's

A swiss army knife of client ready templates and other ops related boilerplate. Conceptually also a great way to by
default export other projects.

- [Helm Charts](charts)
- [Container Images](container-images)
- [Gitlab CI Job Templates](gitlab-templates)
- [Client templates](web-templates)
  - [postgres-react](web-templates/postgres-react)
  - [remix](web-templates/remix)
